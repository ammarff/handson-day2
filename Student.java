public class Student {
    String name;
    String email;
    String phoneNumber;

    public Student(String name, String email, String phoneNumber) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.name = name;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    // public void setName(String name) {
    // this.name = name;
    // }

    // public void setPhoneNumber(String phoneNumber) {
    // this.phoneNumber = phoneNumber;
    // }

    // public void setEmail(String email) {
    // this.email = email;
    // }
}
