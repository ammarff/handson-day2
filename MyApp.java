
import java.util.Scanner;

public class MyApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ketik nama, email & no. HP : ");
        String name = sc.next();
        String email = sc.next();
        String phoneNumber = sc.next();
        Student student = new Student(name, email, phoneNumber);
        System.out.println("Student.getName: " + student.getName());
        System.out.println("Student.getEmail: " + student.getEmail());
        System.out.println("Student.getPhone Number: " + student.getPhoneNumber());
        sc.close();
    }
}
